# Specify Hiring Tests

## Requirements

You will have to do the Algorithm challenge and one of the architecture challenge. If you have any issue during one of the challenges, you can skip it. Just write the implementation you would have done in pseudocode in comments.

The main goal of this test is for us to discuss and understand the mindset you had when developing. Your ability to successfully clear the test may not hinder our discussions.

To send it back, you can:

- send us a git repo link
- send us a zip archive

Do not forget to add a readme so that we can make the app work on our own environments.

You can contact us at anytime if you have an issue or a question at dev@specifyapp.com

## Algorithm challenge (~3h/4h)

### History

Recently we needed to write a function to apply other utils functions like `camelcase` or `rounding` on
specific values from an array of very variable objects.

These objects could have several levels of nesting through subobject or subarray.

To be able to use the utils functions we created an algorithm to list all the paths
which match a specific pattern like `value[*].dimension`.

The result is an array paths where we be able to apply the util function.

### Goal

You have an array of object that match this interface:

```ts
interface Payload {
  [key: string]: Payload | string | number | boolean | Array<Payload | string> | undefined;
}
```

From this array of object, we want to get the path of every property matching the pattern. For example, if you have this array :

```ts
[
  {
    name: 'Random name',
    value: [
      {
        blur: 3,
      },
      {
        blur: 22,
      },
    ],
  },
  {
    name: 'Random name 2',
    value: [
      {
        blur: 77,
      },
    ],
  },
];
```

By using the matching pattern `[*].value[*].blur`, the result would be

```ts
['[0].value[0].blur', '[0].value[1].blur', '[1].value[0].blur'];
```

This function only returns values that match the pattern. Any other value is skipped. Therefor, if no value is matched, the result should be an empty array.

The function will be used on the item, not the whole array.

- Write the function in [./src/index.ts](./src/index.ts) and make it pass the tests from [./tests/listPathByPattern.spec.ts](./tests/listPathByPattern.spec.ts).
- You may not edit the existing tests, but you can add your own tests if you think it's needed.
- This test is about algorithm and logic so you are only allowed to use lodash as an external library.

To check the result, we will execute the tests described in the `tests` folder, and others hidden tests for specifics use cases.

## Architecture challenge

There is minimal constraints so that you can express yourself, you can build it the way you want.

Keep in mind that we will read the code.

When building the challenge, consider us as coworkers that may have to work on the same project.

You can add any features that you consider relevant.

### Frontend

Create a web app with the framework of your choice.
The app must allow to list the files architecture and the content of a file of any public Github repository.
You must think of it has a first step of an app that will scale up in the future and needs to be maintained.

You can use anything: axios, octokit, router, store, sass, tailwind...

### Backend

Create an API with a Database in an easy to launch environment.
This API is a simple crud to manage a user's entity with a password, email, username and any properties of your choice.
You must think of it has a first step of an app that will scale up in the future and needs to be maintained.
