import { defineConfig } from 'vite'
import * as path from 'path'
import Vue from '@vitejs/plugin-vue'
import ViteComponents from 'vite-plugin-components'
import ViteFonts from 'vite-plugin-fonts'
import PurgeIcons from 'vite-plugin-purge-icons'
import VueI18n from '@intlify/vite-plugin-vue-i18n'

/**
 * This is the main configuration file for vitejs
 *
 * @see https://vitejs.dev/config
 */
export default defineConfig({
  // Project root directory (where index.html is located).
  root: process.cwd(),
  // Base public path when served in development or production.
  // You also need to add this base like `history: createWebHistory('my-subdirectory')`
  // in ./src/router.ts
  // base: '/my-subdirectory/',
  base: '/',
  // Directory to serve as plain static assets.
  publicDir: 'public',
  // Adjust console output verbosity.
  logLevel: 'info',
  // Will be passed to @rollup/plugin-alias as its entries option.
  resolve: {
    alias: [
      {
        find: '/~/',
        replacement: `/src/assets/`,
      },
      {
        find: '/@src/',
        replacement: `/src/`,
      },
    ],
  },

  build: {
    sourcemap: process.env.SOURCE_MAP === 'true',
  },
  plugins: [
    /**
     * plugin-vue plugin inject vue library and allow sfc files to work (*.vue)
     *
     * @see https://github.com/vitejs/vite/tree/main/packages/plugin-vue
     */
    Vue({
      include: [/\.vue$/],
    }),

    /**
     * vite-plugin-vue-i18n plugin does i18n resources pre-compilation / optimizations
     *
     * @see https://github.com/intlify/vite-plugin-vue-i18n
     */
    VueI18n({
      include: path.resolve(__dirname, './src/locales/**'),
    }),

    /**
     * vite-plugin-components plugin is responsible of autoloading components
     * documentation and md file are loaded for elements and components sections
     *
     * @see https://github.com/antfu/vite-plugin-components
     */
    ViteComponents({
      extensions: ['vue', 'md'],
      dirs: ['src/components'],
      customLoaderMatcher: (path) => path.endsWith('.md'),
    }),

    /**
     * vite-plugin-purge-icons plugin is responsible of autoloading icones from multiples providers
     *
     * @see https://icones.netlify.app/
     * @see https://github.com/antfu/purge-icons/tree/main/packages/vite-plugin-purge-icons
     */
    PurgeIcons(),

    /**
     * vite-plugin-fonts plugin inject webfonts from differents providers
     *
     * @see https://github.com/stafyniaksacha/vite-plugin-fonts
     */
    ViteFonts({
      google: {
        families: [
          {
            name: 'Titillium Web',
            styles: 'wght@300;400;500;600;700',
          },
          {
            name: 'Roboto',
            styles: 'wght@300;400;500;600;700',
          },
          {
            name: 'Fira Code',
            styles: 'wght@400;600',
          },
        ],
      },
    }),
  ]
})
