import axios from '/@src/axios'
import { AxiosResponse } from 'axios'
import { addDataToPath } from '/@src/utils/path'

export default {
  isRegistered: false,
  namespaced: true,
  state: {
    fileTree: [],
    content: '',
  },
  mutations: {
    ADD_FILES(state: any, payload: any) {
      if (!Object.keys(state.fileTree).length) {
        state.fileTree = payload
        return
      }

      if (payload.length === 0) {
        return
      }

      const path = payload[0].path
      const dirPath = path.substr(0, path.lastIndexOf('/')).split('/')
      state.fileTree = addDataToPath(dirPath, state.fileTree, payload)
    },
    RESET_FILES(state: any) {
      state.fileTree = {}
    },
    SET_CONTENT(state: any, content: string) {
      if (typeof content === 'object') {
        content = JSON.stringify(content)
      }
      state.content = content
    },
    RESET_CONTENT(state: any) {
      state.content = ''
    },
  },
  actions: {
    fetchFilesByRepoName({ commit }: { commit: Function }, name: string): Promise<AxiosResponse> {
      commit('RESET_FILES')
      commit('RESET_CONTENT')
      return new Promise((resolve, reject) => {
        const path: string = ''
        const branch: string = 'master'
        axios
          .get(`/repos/${name}/contents${path}?ref=${branch}`)
          .then((response) => {
            commit('ADD_FILES', response.data)
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    fetchFilesByUrl({ commit }: { commit: Function }, url: string): Promise<AxiosResponse> {
      return new Promise((resolve, reject) => {
        axios
          .get(url)
          .then((response) => {
            commit('ADD_FILES', response.data)
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    fetchFileContentByUrl({ commit }: { commit: Function }, url: string): Promise<AxiosResponse> {
      return new Promise((resolve, reject) => {
        axios
          .get(url)
          .then((response) => {
            commit('SET_CONTENT', response.data)
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
  },
}
