import { createApp } from './app'

import '@purge-icons/generated'
import 'nprogress/nprogress.css'

import './scss/vendors/font-awesome-v5.css'
import './scss/vendors/line-icons-pro.css'

import './scss/main.scss'

createApp({}).then((app) => app.mount('#app'))
