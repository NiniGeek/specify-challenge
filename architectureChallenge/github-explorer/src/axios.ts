import axios, { AxiosInstance } from 'axios'

export const baseURL: string =
  typeof import.meta.env.VITE_API_GITHUB_BASE_URL === 'string'
    ? import.meta.env.VITE_API_GITHUB_BASE_URL
    : ''

export default axios.create({
  baseURL,
})
