import {
  createRouter as createClientRouter,
  createWebHistory,
  RouteRecordRaw,
} from 'vue-router'
import * as NProgress from 'nprogress'

const routes: RouteRecordRaw[] = [
  {
    component: () => import('/src/views/Home.vue'),
    name: 'home',
    path: '/',
  },
  {
    component: () => import('/src/views/NotFound.vue'),
    name: "NotFound",
    path: "/:catchAll(.*)"
  },
]

export function createRouter() {
  const router = createClientRouter({
    history: createWebHistory(),
    routes,
  })

  /**
   * Handle NProgress display on page changes
   */
  router.beforeEach(() => {
    NProgress.start()
  })
  router.afterEach(() => {
    NProgress.done()
  })

  return router
}
