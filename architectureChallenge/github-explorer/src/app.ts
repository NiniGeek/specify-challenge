import {
  createApp as createClientApp,
  h,
  VNode,
  resolveDynamicComponent,
  Transition,
  App,
} from 'vue'
import { RouterView } from 'vue-router'
import { createHead } from '@vueuse/head'
import { createI18n } from './i18n'
import { createRouter } from './router'
import { createStore } from './store'

type AppOptions = {
  enhanceApp?: (app: App) => Promise<void>
}

export async function createApp({ enhanceApp }: AppOptions) {
  const head = createHead()
  const i18n = createI18n()
  const router = createRouter()
  const store = createStore()

  const app = createClientApp({
    // This is the global app setup function
    setup() {
      return () => {
        const defaultSlot = ({ Component: _Component }: any) => {
          const Component = resolveDynamicComponent(_Component) as VNode

          return [
            h(
              Transition,
              { name: 'fade-slow', mode: 'out-in' },
              {
                default: () => [h(Component)],
              }
            ),
          ]
        }

        return h(RouterView, null, {
          default: defaultSlot,
        })
      }
    },
  })

  app.use(head)
  app.use(router)
  app.use(i18n)
  app.use(store)

  if (enhanceApp) {
    await enhanceApp(app)
  }

  return app
}
