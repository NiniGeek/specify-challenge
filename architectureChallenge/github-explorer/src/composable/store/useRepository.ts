import { Store } from 'vuex'
import repository from '/@src/storeModules/repository/store'

export default function useRepository(store: Store<object>) {
  if (!repository.isRegistered) {
    store.registerModule('repository', repository)
    repository.isRegistered = true
  }
}
