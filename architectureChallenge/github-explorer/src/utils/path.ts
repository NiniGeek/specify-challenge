export function addDataToPath(
  pathcomponents: Array<string>,
  arr: Array<any>,
  data: Array<any>
) {
  const component = pathcomponents.shift()
  const comp = arr.find((item) => item.name === component)

  if (pathcomponents.length === 0) {
    comp.children = data
  }

  if (pathcomponents.length) {
    addDataToPath(pathcomponents, comp.children || (comp.children = []), data)
  }
  return arr
}
