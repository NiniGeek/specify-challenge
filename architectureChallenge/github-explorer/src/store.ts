import { createStore as createBaseStore, createLogger } from 'vuex'

const debug = import.meta.env.NODE_ENV !== 'production'

export function createStore() {
  return createBaseStore({
    strict: debug,
    plugins: debug ? [createLogger()] : [],
  })
}
