# Skeleton App

## Requirements
Need docker installed with docker-compose

## Installation

```bash
make install
```

## Usage

### Tree

```bash
.
|-- api       => api server in php/symfony/api platform
|-- client    => client javascript in vue/vuexy
|-- docker    => docker files to run project for development
`-- docs      => documentation in markdown
```

### Development

To run project locally:

```bash
make serve
```

API can be reachable from the following url: https://localhost:8443/docs

### Open Source package

#### Server

- PHP 7.4: [What's new ?](https://kinsta.com/fr/blog/php-7-4/)

- PostgreSQL 11.7 compatible aws RDS et Aurora

#### Api

- [Symfony 5](https://symfony.com/releases): A set of reusable PHP components and a PHP framework for web projects

- [Api platform 2.5](https://api-platform.com): REST and GraphQL framework to build modern API-driven projects

- [Doctrine 2.7](https://symfony.com/doc/current/doctrine.html): Object Relational Mapper

- [Composant Workflow](https://symfony.com/doc/current/components/workflow.html): Tools for managing a workflow or finite state machine.

- [Composant Serialiser](https://symfony.com/doc/current/components/serializer.html): Used to turn objects into a specific format (XML, JSON, YAML, …) and the other way around.

- [Composant Messenger](https://symfony.com/doc/current/components/messenger.html): Helps applications send and receive messages to/from other applications or via message queues

- [Composant Mail](https://symfony.com/doc/current/components/mailer.html): Helps sending emails

- [Puppeteer](https://github.com/puppeteer/puppeteer) via [Browsershot](https://github.com/spatie/browsershot): Convert a webpage to an image or pdf.

- [Flysystem](https://flysystem.thephpleague.com/v1/docs/) via [FlysystemBundle](https://github.com/thephpleague/flysystem-bundle): Efficient abstraction for the filesystem in order to change the storage backend depending on the execution environment (local files in development, cloud storage in production and memory in tests).

- [Opis JSON Schema](https://opis.io/json-schema): PHP implementation for the JSON Schema standard (draft-07 and draft-06), that will help you validate all sorts of JSON documents, whether they are configuration files or a set of data sent to an RESTful API endpoint.

###### Dev

- [Doctrine coding standard](https://github.com/doctrine/coding-standard): A set of [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) rules applied to all Doctrine projects. Doctrine Coding Standard is heavily based on [Slevomat Coding Standard](https://github.com/slevomat/coding-standard).

#### Client

- [Vue 2](https://vuejs.org/): The Progressive JavaScript Framework

- [Vuexy](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/documentation/): the most developer friendly & highly customisable VueJS Admin Dashboard Template based on Vue CLI, Vuex & Vuesax component framework.

- [Sass-node](https://github.com/sass/node-sass) - [Sass](https://sass-lang.com/): the most mature, stable, and powerful professional grade CSS extension language in the world

- [Vuex](https://vuex.vuejs.org/): A state management pattern + library for Vue.js applications. 

- [Vue-router](https://router.vuejs.org/): the official router for Vue.js

- [Vue-i18n](https://kazupon.github.io/vue-i18n/introduction.html): Internationalization plugin of Vue.js

- [Vue-acl](https://github.com/leonardovilarinho/vue-acl#readme): Help you to control the permission of access in your app for yours components and routes

- [VeeValidate](https://logaretm.github.io/vee-validate/): Template Based Form Validation Framework for Vue.js 

###### Dev

- [Cypress](https://www.cypress.io/): Fast, easy and reliable testing for anything that runs in a browser.

#### Docs

- [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet): un langage de balisage léger afin d'offrir une syntaxe facile à lire et à écrire.

- [MkDocs material](https://squidfunk.github.io/mkdocs-material/): Create a branded static site from a set of Markdown files to host the documentation


