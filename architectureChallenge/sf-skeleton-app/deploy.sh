#!/bin/bash

die() {
    echo "ERROR: $1"
    exit 1
}

echo "Build backend"
/bin/bash api/deploy.sh || die "Error cannot build backend"
echo "Build frontend"
/bin/bash client/deploy.sh || die "Error cannot build frontend"