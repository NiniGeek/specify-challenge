#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
    set -- php-fpm "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'php' ] || [ "$1" = 'bin/console' ]; then
    PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-production"
    if [ "$APP_ENV" != 'prod' ]; then
        PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-development"
    fi
    ln -sf "$PHP_INI_RECOMMENDED" "$PHP_INI_DIR/php.ini"

#    lx=$(find /usr/local/lib/php/extensions/ -name xdebug.so | wc -l)
#    if [ "$APP_ENV" != 'prod' ] && [ $lx -eq 0 ]; then
#        apk add --no-cache --virtual .build-deps $PHPIZE_DEPS
#        pecl install xdebug
#        apk del .build-deps
#    fi

    mkdir -p var/cache var/log
    setfacl -R -m u:www-data:rwX -m u:"$(whoami)":rwX var
    setfacl -dR -m u:www-data:rwX -m u:"$(whoami)":rwX var

    if [ "$APP_ENV" != 'prod' ] && [ -f /certs/localCA.crt ]; then
        ln -sf /certs/localCA.crt /usr/local/share/ca-certificates/localCA.crt
        update-ca-certificates
    fi

    if [ "$APP_ENV" != 'prod' ]; then
        composer install --prefer-dist --no-progress --no-suggest --no-interaction
        yarn install
        yarn dev
    fi

    echo "Waiting for db to be ready..."
    until bin/console doctrine:query:sql "SELECT 1" > /dev/null 2>&1; do
        sleep 1
    done

    if ls -A migrations/*.php > /dev/null 2>&1; then
        bin/console doctrine:migrations:migrate --no-interaction
    fi

    if ls -A src/DataFixtures/*.php > /dev/null 2>&1; then
        bin/console doctrine:fixtures:load --no-interaction
    fi

    if ls -A src/translations/*.yaml > /dev/null 2>&1; then
        bin/console lexik:translations:import -g -m -c
    fi
fi

exec docker-php-entrypoint "$@"
