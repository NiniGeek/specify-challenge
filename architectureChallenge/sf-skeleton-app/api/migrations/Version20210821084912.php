<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210821084912 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //
        $this->addSql(<<<EOT
INSERT INTO "user" (id, email, password, username, firstname, lastname, avatar_id)
VALUES ('5834cfb2-a521-4c80-b0ed-a0ae7c0a0c0e', 'admin@interact.lu', '\$2y\$13\$tZvPhuoDde/D5Ap2jYToeO1TCZuQia7iCxnlcLNe1zDLLfGnpohJS', 'admin', 'Super', 'Admin', null);
EOT
        );
        $this->addSql(<<<EOT
INSERT INTO "group" (id, name, roles) VALUES ('d402be78-7dee-4f62-a187-3c4cd0f21ea1', 'Users', 'ROLE_USER');
EOT);
        $this->addSql(<<<EOT
INSERT INTO "group" (id, name, roles) VALUES ('2c29db5a-9789-44c0-a345-101681ae9271', 'Admins', 'ROLE_ADMIN');
EOT);
        $this->addSql(<<<EOT
INSERT INTO "group" (id, name, roles) VALUES ('baa4adf3-ee47-45a0-8199-e4eefbcf26d4', 'Super Admins', 'ROLE_SUPER_ADMIN');
EOT);
        $this->addSql(<<<EOT
INSERT INTO group_user (group_id, user_id)
VALUES ('baa4adf3-ee47-45a0-8199-e4eefbcf26d4', '5834cfb2-a521-4c80-b0ed-a0ae7c0a0c0e');
EOT);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql(<<<EOT
DELETE FROM "user" WHERE id = '5834cfb2-a521-4c80-b0ed-a0ae7c0a0c0e';
EOT
        );
        $this->addSql(<<<EOT
DELETE
FROM group_user
WHERE group_id = 'baa4adf3-ee47-45a0-8199-e4eefbcf26d4'
  AND user_id = '5834cfb2-a521-4c80-b0ed-a0ae7c0a0c0e';
EOT);
        $this->addSql(<<<EOT
DELETE
FROM "group"
WHERE id = 'baa4adf3-ee47-45a0-8199-e4eefbcf26d4';

DELETE
FROM "group"
WHERE id = '2c29db5a-9789-44c0-a345-101681ae9271';

DELETE
FROM "group"
WHERE id = 'd402be78-7dee-4f62-a187-3c4cd0f21ea1';
EOT);
    }
}
