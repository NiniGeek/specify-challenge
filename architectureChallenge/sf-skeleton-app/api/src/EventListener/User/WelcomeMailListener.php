<?php

declare(strict_types=1);

namespace App\EventListener\User;

use App\Entity\User\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class WelcomeMailListener
{
    private MailerInterface $mailer;
    private string $mailFrom;

    public function __construct(
        MailerInterface $mailer,
        string $mailFrom
    ) {
        $this->mailer   = $mailer;
        $this->mailFrom = $mailFrom;
    }

    public function sendWelcomeEmailToUser(ViewEvent $event): void
    {
        $user   = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (! $user instanceof User || $method !== Request::METHOD_POST) {
            return;
        }

        $email = (new TemplatedEmail())
            ->from($this->mailFrom)
            ->to(new Address($user->getEmail(), $user->getName()))
            ->subject('Welcome !')
            ->htmlTemplate('email/user/welcome.html.twig')
            ->context([
                'user_username' => $user->getUsername(),
                'user_name' => $user->getName(),
                'user_plainPassword' => $user->getPlainPassword(),
                'importance' => 'High',
            ]);

        $this->mailer->send($email);
    }
}
