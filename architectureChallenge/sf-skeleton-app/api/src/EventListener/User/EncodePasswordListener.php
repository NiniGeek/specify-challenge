<?php

declare(strict_types=1);

namespace App\EventListener\User;

use App\Entity\User\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class EncodePasswordListener
{
    private UserPasswordHasherInterface $passwordEncoder;

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function prePersist(User $user): void
    {
        $this->encodePassword($user);
    }

    public function preUpdate(User $user): void
    {
        $this->encodePassword($user);
    }

    protected function encodePassword(User $user): void
    {
        if (! $user->getPlainPassword()) {
            return;
        }

        $encoded = $this->passwordEncoder->hashPassword(
            $user,
            $user->getPlainPassword()
        );

        $user->setPassword($encoded);
    }
}
