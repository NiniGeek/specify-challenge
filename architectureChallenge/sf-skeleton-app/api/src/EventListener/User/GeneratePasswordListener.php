<?php

declare(strict_types=1);

namespace App\EventListener\User;

use App\Entity\User\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class GeneratePasswordListener
{
    public function generatePassword(ViewEvent $event): void
    {
        $user   = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (! $user instanceof User || $method !== Request::METHOD_POST) {
            return;
        }

        $user->generatePlainPassword(12);
    }
}
