<?php

declare(strict_types=1);

namespace App\EventListener\Auth;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Serializer\SerializerInterface;

use function array_unique;

class JWTCreatedListener
{
    private SerializerInterface $serializer;

    private RoleHierarchyInterface $roleHierarchy;

    public function __construct(SerializerInterface $serializer, RoleHierarchyInterface $roleHierarchy)
    {
        $this->serializer    = $serializer;
        $this->roleHierarchy = $roleHierarchy;
    }

    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $payload  = $event->getData();
        $user     = $event->getUser();
        $allRoles = array_unique($this->roleHierarchy->getReachableRoleNames($user->getRoles()));

        $payload['user']  = $this->serializer->normalize($user, 'jsonld');
        $payload['roles'] = $allRoles;

        $event->setData($payload);
    }
}
