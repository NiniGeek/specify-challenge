<?php

declare(strict_types=1);

namespace App\Swagger;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model;
use ApiPlatform\Core\OpenApi\OpenApi;
use ArrayObject;

final class JwtDecorator implements OpenApiFactoryInterface
{
    private OpenApiFactoryInterface $decorated;

    public function __construct(OpenApiFactoryInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Token'] = [
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'refresh_token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ];

        $schemas['Credentials'] = [
            'type' => 'object',
            'properties' => [
                'username' => [
                    'type' => 'string',
                    'example' => 'user',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => 'password',
                ],
            ],
        ];

        $schemas['RefreshToken'] = [
            'type' => 'object',
            'properties' => [
                'refresh_token' => [
                    'type' => 'string',
                    'example' => 'xxxx4b54b0076d2fcc5a51a6e60c0fb83b0bc90b47e2c886accb70850795ff5fc6e9cb362xxxx',
                ],
            ],
        ];

        $pathItem = new Model\PathItem(
            'JWT Token',
            null,
            null,
            null,
            null,
            new Model\Operation(
                'postCredentialsItem',
                ['Token'],
                [
                    '200' => [
                        'description' => 'Get JWT token',
                        'content' => [
                            'application/json' => [
                                'schema' => ['$ref' => '#/components/schemas/Token'],
                            ],
                        ],
                    ],
                ],
                'Get JWT token to login.',
                '',
                null,
                [],
                new Model\RequestBody(
                    'Create new JWT Token',
                    new ArrayObject([
                        'application/json' => [
                            'schema' => ['$ref' => '#/components/schemas/Credentials'],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/authentication', $pathItem);

        $pathItem = new Model\PathItem(
            'JWT Token',
            null,
            null,
            null,
            null,
            new Model\Operation(
                'postTokenRefreshItem',
                ['Token'],
                [
                    '200' => [
                        'description' => 'Get new JWT token and new refresh_token',
                        'content' => [
                            'application/json' => [
                                'schema' => ['$ref' => '#/components/schemas/Token'],
                            ],
                        ],
                    ],
                ],
                'Refresh token.',
                '',
                null,
                [],
                new Model\RequestBody(
                    'Create new JWT Token',
                    new ArrayObject([
                        'application/json' => [
                            'schema' => ['$ref' => '#/components/schemas/RefreshToken'],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/authentication/refresh', $pathItem);

        return $openApi;
    }
}
