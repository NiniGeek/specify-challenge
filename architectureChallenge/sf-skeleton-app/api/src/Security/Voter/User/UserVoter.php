<?php

declare(strict_types=1);

namespace App\Security\Voter\User;

use App\Entity\User\User;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

use function in_array;

class UserVoter extends Voter
{
    public const LIST       = 'USER_VOTER_LIST';
    public const VIEW       = 'USER_VOTER_VIEW';
    public const ADD        = 'USER_VOTER_ADD';
    public const ADD_ADMIN  = 'USER_VOTER_ADD_ADMIN';
    public const EDIT       = 'USER_VOTER_EDIT';
    public const EDIT_SELF  = 'USER_VOTER_EDIT_SELF';
    public const EDIT_ADMIN = 'USER_VOTER_EDIT_ADMIN';
    public const DELETE     = 'USER_VOTER_DELETE';

    private AuthorizationCheckerInterface $security;

    private AccessDecisionManagerInterface $accessDecisionManager;

    public function __construct(AuthorizationCheckerInterface $security, AccessDecisionManagerInterface $accessDecisionManager)
    {
        $this->security              = $security;
        $this->accessDecisionManager = $accessDecisionManager;
    }

    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [
            self::LIST,
            self::VIEW,
            self::ADD,
            self::ADD_ADMIN,
            self::EDIT,
            self::EDIT_SELF,
            self::EDIT_ADMIN,
            self::DELETE,
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (! $user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::LIST:
                return $this->canList($subject, $token);

            case self::VIEW:
                return $this->canView($subject, $token);

            case self::ADD:
                return $this->canAdd($subject, $token);

            case self::ADD_ADMIN:
                return $this->canAddAdmin($subject, $token);

            case self::EDIT:
                return $this->canEdit($subject, $token);

            case self::EDIT_SELF:
                return $this->canEditSelf($subject, $token);

            case self::EDIT_ADMIN:
                return $this->canEditAdmin($subject, $token);

            case self::DELETE:
                return $this->canDelete($subject, $token);
        }

        throw new LogicException('This code should not be reached!');
    }

    private function canList(?User $user, TokenInterface $token): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    private function canView(?User $user, TokenInterface $token): bool
    {
        if ($this->canEditSelf($user, $token)) { // self
            return true;
        }

        return $this->canList($user, $token);
    }

    private function canAdd(?User $user, TokenInterface $token): bool
    {
        return $this->canList($user, $token);
    }

    private function canEdit(?User $user, TokenInterface $token): bool
    {
        if ($this->canEditSelf($user, $token)) {
            return true;
        }

        return $this->canEditAdmin($user, $token);
    }

    private function canEditSelf(?User $user, TokenInterface $token): bool
    {
        return $token->getUser() === $user;
    }

    private function canEditAdmin(?User $user, TokenInterface $token): bool
    {
        return $this->canList($user, $token);
    }

    private function canAddAdmin(?User $user, TokenInterface $token): bool
    {
        return $this->canList($user, $token);
    }

    private function canDelete(?User $user, TokenInterface $token): bool
    {
        if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
            return true;
        }

        $targetedUserToken = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        if ($this->accessDecisionManager->decide($targetedUserToken, ['ROLE_SUPER_ADMIN'])) {
            return false;
        }

        return $this->canList($user, $token);
    }
}
