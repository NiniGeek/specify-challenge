<?php

declare(strict_types=1);

namespace App\Security\Voter\User;

use App\Entity\User\Group;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

use function in_array;

class GroupVoter extends Voter
{
    public const LIST             = 'GROUP_VOTER_LIST';
    public const LIST_SUPER_ADMIN = 'GROUP_VOTER_LIST_SUPER_ADMIN';
    public const VIEW             = 'GROUP_VOTER_VIEW';
    public const ADD              = 'GROUP_VOTER_ADD';
    public const EDIT             = 'GROUP_VOTER_EDIT';
    public const DELETE           = 'GROUP_VOTER_DELETE';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
            self::LIST,
            self::LIST_SUPER_ADMIN,
            self::VIEW,
            self::ADD,
            self::EDIT,
            self::DELETE,
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (! $user instanceof UserInterface) {
            // if the user is anonymous, do not grant access
            return false;
        }

        switch ($attribute) {
            case self::LIST:
                return $this->canList($subject, $token);

            case self::LIST_SUPER_ADMIN:
                return $this->canListSuperAdmin($subject, $token);

            case self::VIEW:
                return $this->canView($subject, $token);

            case self::ADD:
                return $this->canAdd($subject, $token);

            case self::EDIT:
                return $this->canEdit($subject, $token);

            case self::DELETE:
                return $this->canDelete($subject, $token);
        }

        throw new LogicException('This code should not be reached!');
    }

    private function canList(?Group $group, TokenInterface $token): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    private function canListSuperAdmin(?Group $group, TokenInterface $token): bool
    {
        return $this->security->isGranted('ROLE_SUPER_ADMIN');
    }

    private function canView(?Group $group, TokenInterface $token): bool
    {
        if (! $this->canListSuperAdmin($group, $token)) {
            if (in_array('ROLE_SUPER_ADMIN', $group->getRoles(), true)) {
                return false;
            }
        }

        return $this->canList($group, $token);
    }

    private function canAdd(?Group $group, TokenInterface $token): bool
    {
        return $this->canView($group, $token);
    }

    private function canEdit(?Group $group, TokenInterface $token): bool
    {
        return $this->canView($group, $token);
    }

    private function canDelete(?Group $group, TokenInterface $token): bool
    {
        return $this->canView($group, $token);
    }
}
