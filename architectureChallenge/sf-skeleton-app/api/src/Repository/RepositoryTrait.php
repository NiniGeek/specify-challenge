<?php

declare(strict_types=1);

namespace App\Repository;

trait RepositoryTrait
{
    /**
     * @param mixed $entity
     *
     * @return mixed
     */
    public function save($entity, bool $flush = true)
    {
        $this->getEntityManager()->persist($entity);
        if ($flush) {
            $this->flush();
        }

        return $entity;
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }
}
