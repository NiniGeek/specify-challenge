<?php

declare(strict_types=1);

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Entity\User\User;
use App\Manager\User\UserManager;
use App\Security\Voter\User\UserVoter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ContextBuilder implements SerializerContextBuilderInterface
{
    use ContextBuilderTrait;

    private UserManager $userManager;

    public function __construct(
        SerializerContextBuilderInterface $decorated,
        AuthorizationCheckerInterface $authorizationChecker,
        UserManager $userManager
    ) {
        $this->decorated            = $decorated;
        $this->authorizationChecker = $authorizationChecker;
        $this->userManager          = $userManager;
    }

    /**
     * @return string[string]
     *
     * @inheritDoc
     */
    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context       = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;

        switch ($resourceClass) {
            case User::class:
                return $this->handleUser($context, $normalization);
        }

        return $context;
    }

    /**
     * @param string[][] $context
     *
     * @return string[][]
     */
    private function handleUser(array $context, bool $normalization): array
    {
        if ($this->isEditAction($context, $normalization)) {
            $user = $this->userManager->getResource($context['request_uri']);
            if ($this->isGranted(UserVoter::EDIT_SELF, $user)) {
                $context['groups'][] = 'user:self_write';
            }

            if ($this->isGranted(UserVoter::EDIT_ADMIN)) {
                $context['groups'][] = 'user:admin_write';
            }
        }

        if ($this->isAddAction($context, $normalization) && $this->isGranted(UserVoter::ADD_ADMIN)) {
            $context['groups'][] = 'user:admin_write';
        }

        return $context;
    }
}
