<?php

declare(strict_types=1);

namespace App\Serializer;

use App\Entity\User\User;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

class UserNormalizer implements NormalizerAwareInterface, ContextAwareNormalizerInterface
{
    use NormalizerAwareTrait;

    protected const ALREADY_CALLED = 'USER_NORMALIZER_ALREADY_CALLED';

    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->urlGenerator = $urlGenerator;
    }

    protected function getAlreadyCalled(): string
    {
        return self::ALREADY_CALLED;
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        if (isset($context[$this->getAlreadyCalled()])) {
            return false;
        }

        if (! $data instanceof User) {
            return false;
        }

        return $data instanceof User;
    }

    /**
     * @inheritDoc
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $context[$this->getAlreadyCalled()] = true;

        $data               = $this->normalizer->normalize($object, $format, $context);
        $file               = $object->getAvatar();
        $data['fileAvatar'] = null;
        if ($file !== null) {
            $data['fileAvatar'] = $this->urlGenerator->generate(
                'get_avatar',
                ['id' => $object->getId(), 'filename' => $file->getFileOriginalName()],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
        }

        return $data;
    }
}
