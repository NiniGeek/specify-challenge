<?php

declare(strict_types=1);

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

trait ContextBuilderTrait
{
    private SerializerContextBuilderInterface $decorated;

    private AuthorizationCheckerInterface $authorizationChecker;

    public function __construct(
        SerializerContextBuilderInterface $decorated,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->decorated            = $decorated;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param string[][] $context
     */
    protected function isListAction(array $context, bool $normalization): bool
    {
        return $this->isNormalization($normalization) &&
            $this->isMethod('get', $context) &&
            $this->isOperationCollection($context);
    }

    /**
     * @param string[][] $context
     */
    protected function isViewAction(array $context, bool $normalization): bool
    {
        return $this->isNormalization($normalization) &&
            $this->isMethod('get', $context) &&
            $this->isOperationItem($context);
    }

    /**
     * @param string[][] $context
     */
    protected function isAddAction(array $context, bool $normalization): bool
    {
        return $this->isDenormalization($normalization) &&
            $this->isMethod('post', $context) &&
            $this->isOperationCollection($context);
    }

    /**
     * @param string[][] $context
     */
    protected function isEditAction(array $context, bool $normalization): bool
    {
        return $this->isDenormalization($normalization) &&
            $this->isMethod('put', $context) &&
            $this->isOperationItem($context);
    }

    /**
     * @param string[][] $context
     */
    protected function isDeleteAction(array $context, bool $normalization): bool
    {
        return $this->isNormalization($normalization) &&
            $this->isMethod('delete', $context) &&
            $this->isOperationItem($context);
    }

    /**
     * @param object|array|null $subject
     */
    protected function isGranted(string $attribute, $subject = null): bool
    {
        return $this->authorizationChecker->isGranted($attribute, $subject);
    }

    private function isNormalization(bool $normalization): bool
    {
        return $normalization === true;
    }

    private function isDenormalization(bool $normalization): bool
    {
        return ! $this->isNormalization($normalization);
    }

    /**
     * @param string[][] $context
     */
    private function isMethod(string $method, array $context): bool
    {
        if ($this->isOperationItem($context)) {
            return $method === $context['item_operation_name'];
        }

        if ($this->isOperationCollection($context)) {
            return $method === $context['collection_operation_name'];
        }

        return false;
    }

    /**
     * @param string[][] $context
     */
    private function isOperationItem(array $context): bool
    {
        return $context['operation_type'] === 'item';
    }

    /**
     * @param string[][] $context
     */
    private function isOperationCollection(array $context): bool
    {
        return $context['operation_type'] === 'collection';
    }
}
