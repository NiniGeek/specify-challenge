<?php

declare(strict_types=1);

namespace App\Filter\User;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Security\Voter\User\GroupVoter;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

final class GroupFilter extends AbstractContextAwareFilter
{
    private Security $security;

    /**
     * @param mixed[] $properties
     */
    public function __construct(
        ManagerRegistry $managerRegistry,
        Security $security,
        ?RequestStack $requestStack = null,
        ?LoggerInterface $logger = null,
        ?array $properties = null,
        ?NameConverterInterface $nameConverter = null
    ) {
        parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);
        $this->security = $security;
    }

    /**
     * @param mixed $value
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?string $operationName = null
    ): void {
        if ($this->security->isGranted(GroupVoter::LIST_SUPER_ADMIN)) {
            return;
        }

        $queryBuilder
            ->andWhere('o.roles NOT LIKE :role')
            ->setParameter('role', '%ROLE_SUPER_ADMIN%');
    }

    /**
     * @return mixed[]
     */
    public function getDescription(string $resourceClass): array
    {
        return [];
    }
}
