<?php

declare(strict_types=1);

namespace App\Manager\User;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\User\User;
use App\Manager\ManagerTrait;
use App\Repository\User\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class UserManager
{
    use ManagerTrait;

    private UserRepository $repository;
    private IriConverterInterface $iriConverter;
    private JWTTokenManagerInterface $JWTManager;
    private MailerInterface $mailer;
    private string $frontEndUrl;
    private string $mailFrom;

    public function __construct(
        EntityManagerInterface $entityManager,
        IriConverterInterface $iriConverter,
        JWTTokenManagerInterface $JWTManager,
        MailerInterface $mailer,
        string $frontEndUrl,
        string $mailFrom
    ) {
        $this->repository   = $entityManager->getRepository(User::class);
        $this->iriConverter = $iriConverter;
        $this->JWTManager   = $JWTManager;
        $this->mailer       = $mailer;
        $this->frontEndUrl  = $frontEndUrl;
        $this->mailFrom     = $mailFrom;
    }

    public function getResource(?string $iri): ?User
    {
        if ($iri === null) {
            return null;
        }

        $obj = $this->iriConverter->getItemFromIri($iri);
        if ($obj instanceof User) {
            return $obj;
        }

        return null;
    }

    /**
     * @return User[]
     */
    public function getPricingPricingEditors(): array
    {
        return $this->repository->findValidPricingPricingEditors();
    }

    /**
     * @return User[]
     */
    public function getPricingSheetEditors(): array
    {
        return $this->repository->findValidPricingSheetEditors();
    }

    /**
     * @return User[]
     */
    public function getPricingPricingValidators(): array
    {
        return $this->repository->findValidPricingPricingValidators();
    }

    /**
     * @return User[]
     */
    public function getPricingSheetValidators(): array
    {
        return $this->repository->findValidPricingSheetValidators();
    }

    /**
     * @return User[]
     */
    public function getPricingPublisherValidators(): array
    {
        return $this->repository->findValidPricingPublisherValidators();
    }

    public function findOneByUsername(string $username): ?User
    {
        return $this->repository->findOneBy(['username' => $username]);
    }

    public function sendResetInvitation(User $user): void
    {
        $token = $this->JWTManager->create($user);

        //send mail
        $email = (new TemplatedEmail())
            ->from($this->mailFrom)
            ->to(new Address($user->getEmail(), $user->getName()))
            ->subject('[' . $user->getName() . '] Changement de mot de passe')
            ->htmlTemplate('email/security/reset_password_invitation.html.twig')
            ->context([
                'user_name' => $user->getName(),
                'front_end_reset_invitation_url' => $this->getFrontEndResetInvitationUrl($token),
                'importance' => 'high',
            ]);

        $this->mailer->send($email);
    }

    public function getFrontEndResetInvitationUrl(string $token): string
    {
        return $this->frontEndUrl . '/reset/password/?token=' . $token;
    }
}
