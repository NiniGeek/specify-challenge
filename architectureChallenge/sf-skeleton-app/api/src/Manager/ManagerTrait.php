<?php

declare(strict_types=1);

namespace App\Manager;

trait ManagerTrait
{
    public function save(object $entity, bool $flush = true): object
    {
        return $this->repository->save($entity, $flush);
    }

    public function flush(): void
    {
        $this->repository->flush();
    }

    public function get($id): ?object
    {
        return $this->repository->find($id);
    }
}
