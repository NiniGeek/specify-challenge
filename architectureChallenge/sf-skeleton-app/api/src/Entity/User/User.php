<?php

declare(strict_types=1);

namespace App\Entity\User;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Doctrine\Uuid\UuidGenerator;
use App\Entity\MediaObject;
use App\Repository\User\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use RangeException;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

use function array_unique;
use function implode;
use function mb_strlen;
use function random_int;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 *
 * @UniqueEntity("email")
 * @ApiResource(
 *     normalizationContext={"groups"={"user", "user:read"}},
 *     denormalizationContext={"groups"={"user", "user:write"}},
 *     collectionOperations={
 *          "get" = {
 *              "security_post_denormalize" = "is_granted('USER_VOTER_LIST')",
 *              "normalization_context"={"groups"={"user", "user:read", "user:read_list"}}
 *          },
 *          "post" = { "security_post_denormalize" = "is_granted('USER_VOTER_ADD', object)" }
 *     },
 *     itemOperations={
 *          "get" = { "security" = "is_granted('USER_VOTER_VIEW', object)" },
 *          "put" = { "security" = "is_granted('USER_VOTER_EDIT', object)" },
 *          "patch" = { "security" = "is_granted('USER_VOTER_EDIT', object)" },
 *          "delete" = { "security" = "is_granted('USER_VOTER_DELETE', object)" },
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={
 *     "email",
 *     "username",
 *     "firstname",
 *     "lastname",
 *     "groups.name"
 * })
 * @ApiFilter(SearchFilter::class, properties={
 *     "email": "partial",
 *     "username": "partial",
 *     "firstname": "partial",
 *     "lastname": "partial",
 *     "groups.name": "partial"
 * })
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    public const ROLE_ADMIN       = 'ROLE_ADMIN';
    public const ROLE_USER        = 'ROLE_USER';

    // not a user
    public const ROLE_COMMAND = 'ROLE_COMMAND';

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     *
     * @Groups({"user"})
     */
    private ?UuidInterface $id = null;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Groups({"user"})
     */
    private string $email;

    /** @ORM\Column(type="string") */
    private ?string $password;

    /** @Groups({"user:self_write"}) */
    private ?string $plainPassword = null;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"user"})
     */
    private string $username;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"user"})
     */
    private string $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"user"})
     */
    private string $lastname;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, mappedBy="users")
     *
     * @Groups({"user:read", "user:admin_write"})
     * @var Collection|Group[]
     */
    private Collection $groups;

    /**
     * @ORM\JoinColumn(nullable=true)
     * @ORM\OneToOne(targetEntity=MediaObject::class, cascade={"persist", "remove"}, fetch="EAGER")
     *
     * @Groups({"user"})
     */
    private ?MediaObject $avatar = null;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function setId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @inheritDoc UserInterface
     */
    public function getRoles(): array
    {
        $roles = [self::ROLE_USER];

        foreach ($this->getGroups() as $group) {
            foreach ($group->getRoles() as $role) {
                $roles[] = $role;
            }
        }

        return array_unique($roles);
    }

    /**
     * @inheritDoc PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
        $this->password      = null;
    }

    public function generatePlainPassword(
        int $length = 64,
        string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ): void {
        if ($length < 1) {
            throw new RangeException('Length must be a positive integer');
        }

        $pieces = [];
        $max    = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }

        $this->setPlainPassword(implode('', $pieces));
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): void
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @Groups({"user"})
     */
    public function getName(): string
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (! $this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->addUser($this);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
            $group->removeUser($this);
        }

        return $this;
    }

    public function getAvatar(): ?MediaObject
    {
        return $this->avatar;
    }

    public function setAvatar(?MediaObject $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }
}
