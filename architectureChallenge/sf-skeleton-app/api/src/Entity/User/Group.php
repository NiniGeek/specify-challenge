<?php

declare(strict_types=1);

namespace App\Entity\User;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Doctrine\Uuid\UuidGenerator;
use App\Repository\User\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`group`")
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"group", "group:read"}},
 *     denormalizationContext={"groups"={"group", "group:write"}},
 *     collectionOperations={
 *         "get" = { "security_post_denormalize" = "is_granted('GROUP_VOTER_LIST')" },
 *         "post" = { "security_post_denormalize" = "is_granted('GROUP_VOTER_ADD', object)" }
 *     },
 *     itemOperations={
 *         "get" = { "security" = "is_granted('GROUP_VOTER_VIEW', object)" },
 *         "put" = { "security" = "is_granted('GROUP_VOTER_EDIT', object)" },
 *         "patch" = { "security" = "is_granted('GROUP_VOTER_EDIT', object)" },
 *         "delete" = { "security" = "is_granted('GROUP_VOTER_DELETE', object)" }
 *     },
 *     attributes={
 *         "pagination_client_enabled"=true,
 *         "filters"={"App\Filter\GroupFilter"}
 *     }
 * )
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     *
     * @Groups({"group", "user:read_list"})
     */
    private ?UuidInterface $id = null;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Groups({"group", "user:read_list"})
     */
    private string $name;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     *
     * @Groups({"group"})
     *
     * @var string[]
     */
    private array $roles;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="groups")
     *
     * @ApiSubresource()
     *
     * @var User[]|Collection
     */
    private Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->roles = [];
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function setId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string[]|Collection|null
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @param string[] $roles
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (! $this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }
}
