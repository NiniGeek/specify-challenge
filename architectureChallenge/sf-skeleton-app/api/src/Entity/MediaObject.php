<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateMediaObjectAction;
use App\Doctrine\Uuid\UuidGenerator;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 *
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     normalizationContext={"groups"={"media_object", "media_object:read"}},
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreateMediaObjectAction::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "media_object_create"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         }
 *     },
 *     itemOperations={
 *         "get",
 *         "delete",
 *         "put"
 *     }
 * )
 */
class MediaObject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     *
     * @Groups({
     *     "media_object:read",
     *     "user:read"
     * })
     */
    private ?UuidInterface $id = null;

    /**
     * @ApiProperty(iri="http://schema.org/contentUrl")
     * @Groups({
     *     "media_object:read",
     *     "user:read",
     * })
     */
    public ?string $contentUrl = null;

    /**
     * @Assert\NotNull(groups={"media_object_create"})
     * @Vich\UploadableField(
     *     mapping="media_object",
     *     fileNameProperty="filePath",
     *     originalName="fileOriginalName",
     *     mimeType="fileMimeType",
     *     size="fileSize",
     *     dimensions="fileDimensions"
     * )
     */
    public ?File $file = null;

    /** @ORM\Column(nullable=true) */
    public ?string $filePath = null;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"media_object:read"})
     */
    private ?string $fileOriginalName = null;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"media_object:read"})
     */
    private ?string $fileMimeType = null;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"media_object:read"})
     */
    private ?int $fileSize = null;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     *
     * @Groups({"media_object:read"})
     * @var int[]|null
     */
    private ?array $fileDimensions = null;

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function setId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFileOriginalName(): ?string
    {
        return $this->fileOriginalName;
    }

    public function setFileOriginalName(?string $fileOriginalName): self
    {
        $this->fileOriginalName = $fileOriginalName;

        return $this;
    }

    public function getFileMimeType(): ?string
    {
        return $this->fileMimeType;
    }

    public function setFileMimeType(?string $fileMimeType): self
    {
        $this->fileMimeType = $fileMimeType;

        return $this;
    }

    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    public function setFileSize(?int $fileSize): self
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * @return int[]|null
     */
    public function getFileDimensions(): ?array
    {
        return $this->fileDimensions;
    }

    /**
     * @param int[]|null $fileDimensions
     */
    public function setFileDimensions(?array $fileDimensions): self
    {
        $this->fileDimensions = $fileDimensions;

        return $this;
    }

    public function __sleep()
    {
        return ['id', 'contentUrl', 'filePath', 'fileOriginalName', 'fileMimeType', 'fileSize', 'fileDimensions'];
    }
}
