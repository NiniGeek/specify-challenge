<?php

declare(strict_types=1);

namespace App\Doctrine\Uuid;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use ReflectionClass;
use ReflectionProperty;

final class UuidGenerator extends AbstractIdGenerator
{
    /**
     * @param mixed $entity
     */
    public function generate(EntityManager $entityManager, $entity): UuidInterface
    {
        $idReflectionProperty = $this->findClassIdProperty($entity);
        if ($idReflectionProperty === null) {
            return Uuid::uuid4();
        }

        return $idReflectionProperty->getValue($entity) ?? Uuid::uuid4();
    }

    private function findClassIdProperty(object $object): ?ReflectionProperty
    {
        $reflectionClass = new ReflectionClass($object);

        do {
            if ($reflectionClass->hasProperty('id')) {
                $idReflectionProperty = $reflectionClass->getProperty('id');
                $idReflectionProperty->setAccessible(true);

                return $idReflectionProperty;
            }

            $reflectionClass = $reflectionClass->getParentClass();
        } while ($reflectionClass);

        return null;
    }
}
