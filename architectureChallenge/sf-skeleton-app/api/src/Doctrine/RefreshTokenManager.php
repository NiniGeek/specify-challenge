<?php

declare(strict_types=1);

namespace App\Doctrine;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshTokenRepository;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManager as BaseRefreshTokenManager;

class RefreshTokenManager extends BaseRefreshTokenManager
{
    protected EntityManagerInterface $objectManager;

    protected string $class;

    protected RefreshTokenRepository $repository;

    public function __construct(ObjectManager $om, string $class)
    {
        $this->objectManager = $om;
        $this->repository    = $om->getRepository($class);
        $metadata            = $om->getClassMetadata($class);
        $this->class         = $metadata->getName();
    }

    /**
     * @inheritDoc
     */
    public function get($refreshToken): ?RefreshTokenInterface
    {
        return $this->repository->findOneBy(['refreshToken' => $refreshToken]);
    }

    /**
     * @inheritDoc
     */
    public function getLastFromUsername($username): ?RefreshTokenInterface
    {
        return $this->repository->findOneBy(['username' => $username], ['valid' => 'DESC']);
    }

    /**
     * @param bool|true $andFlush
     */
    public function save(RefreshTokenInterface $refreshToken, bool $andFlush = true): void
    {
        $this->objectManager->persist($refreshToken);

        if (! $andFlush) {
            return;
        }

        $this->objectManager->flush();
    }

    public function delete(RefreshTokenInterface $refreshToken, bool $andFlush = true): void
    {
        $this->objectManager->remove($refreshToken);

        if (! $andFlush) {
            return;
        }

        $this->objectManager->flush();
    }

    /**
     * @return RefreshTokenInterface[]
     */
    public function revokeAllInvalid(?DateTime $datetime = null, bool $andFlush = true): array
    {
        $invalidTokens = $this->repository->findInvalid($datetime);

        foreach ($invalidTokens as $invalidToken) {
            $this->objectManager->remove($invalidToken);
        }

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return $invalidTokens;
    }

    /**
     * Returns the RefreshToken fully qualified class name.
     */
    public function getClass(): string
    {
        return $this->class;
    }
}
