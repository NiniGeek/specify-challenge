<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Manager\User\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    /**
     * @Route("/resetting/request/{username}", name="resetting_request")
     */
    public function resetRequestAction(UserManager $userManager, string $username = ''): Response
    {
        $user = $userManager->findOneByUsername($username);
        if (! $user) {
            throw $this->createNotFoundException();
        }

        $userManager->sendResetInvitation($user);

        return new Response();
    }
}
