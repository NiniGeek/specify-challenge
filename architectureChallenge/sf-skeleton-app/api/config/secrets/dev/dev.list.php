<?php

declare(strict_types=1);

return [
    'JWT_PASSPHRASE' => null,
    'JWT_PUBLIC_KEY' => null,
    'JWT_SECRET_KEY' => null,
];
