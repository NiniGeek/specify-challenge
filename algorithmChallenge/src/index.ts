// You can only use lodash
import {has, get} from 'lodash'

export default function listPathsByPattern<T extends object>(base: T | Array<T>, pattern: string): Array<string> {
    if (pattern === '' || !pattern.includes('[*]')) {
        return has(base, pattern)? [pattern] : []
    }

    const tokenizedList = pattern.split(/(?=\[\*])|(?<=\[\*])/g)

    console.log(tokenizedList)
    console.log(base)

    function patternResolver (base: T | Array<T>, tokenizedList: Array<string>, path: string = '', pathList: Array<string> = []): Array<string> {
        if (tokenizedList.length === 0) {
            pathList.push(path)
            return pathList;
        }

        const token = tokenizedList.shift();
        if (token === '[*]') {
            if (!Array.isArray(base)) {
                return [];
            }
            let result = []
            for (const [index, value] of base.entries()) {
                let currentPath = `${path}[${index}]`
                result.push(patternResolver(value, tokenizedList.slice(), currentPath, pathList))
            }

            return pathList.concat(...result)
        }

        if (token && has(base, token.substring(1))) {
            let currentPath = `${path}${token}`
            let value = get(base, token)

            if (Array.isArray(value)) {
                return patternResolver(value, tokenizedList.slice(), currentPath, pathList)
            }
            return [currentPath];
        }


        return pathList
    }

    const data = patternResolver(base, tokenizedList)
    console.log(data)
    return data
}
