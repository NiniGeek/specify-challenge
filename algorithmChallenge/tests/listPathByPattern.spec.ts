import listPathsByPattern from '../src';
import _ from 'lodash';

async function seeds() {
  return (await import('./seeds.json')).default;
}

describe('List path by pattern', () => {
  it.only('Should loop over a unique array containing some arrays (test presented in the readme)', async done => {
    const pattern = '[*].value[*].blur';
    const customSeeds = [
      {
        name: 'Random name',
        value: [
          {
            blur: 3,
          },
          {
            blur: 22,
          },
        ],
      },
      {
        name: 'Random name 2',
        value: [
          {
            blur: 77,
          },
        ],
      },
    ];
    const expected = ['[0].value[0].blur', '[0].value[1].blur', '[1].value[0].blur'];

    const result = listPathsByPattern(customSeeds, pattern);
    expect(result.length).toEqual(3);
    expect(result).toEqual(expected);
    done();
  });

  it('Should return as many elements as there are in the shadow array', async done => {
    const tokens: Array<Payload> = await seeds();
    const pattern = 'value[*].blur.value.measure';
    const expected = [
      ['value[0].blur.value.measure'],
      ['value[0].blur.value.measure', 'value[1].blur.value.measure'],
      [
        'value[0].blur.value.measure',
        'value[1].blur.value.measure',
        'value[2].blur.value.measure',
        'value[3].blur.value.measure',
        'value[4].blur.value.measure',
        'value[5].blur.value.measure',
      ],
    ];

    tokens
      .filter(({ type }) => type === 'shadow')
      .forEach((token, index) => {
        const result = listPathsByPattern(token, pattern);
        expect((token.value as Array<Payload>).length).toEqual(result.length);
        expect(result).toEqual(expected[index]);
      });
    done();
  });

  it('Should return as many elements as there are in the shadow array', async done => {
    const tokens: Array<Payload> = await seeds();
    const pattern = 'value[*].blur.value.measure';
    tokens
      .filter(({ type }) => type === 'shadow')
      .forEach(token => {
        const result = listPathsByPattern(token, pattern);
        expect((token.value as Array<Payload>).length).toEqual(result.length);
        result.forEach((elm, index) => {
          expect(elm).toEqual(pattern.replace('[*]', `[${index}]`));
        });
      });
    done();
  });

  it('Should loop over multiple array and return as many element as there are in the payload ', async done => {
    const pattern = '[*][*].name';
    const customSeeds = [
      [
        {
          name: 1,
        },

        {
          name: 2,
        },
      ],
      [
        {
          name: 3,
        },

        {
          name: 4,
        },
      ],
    ];
    const result = listPathsByPattern(customSeeds, pattern);
    expect(result.length).toEqual(customSeeds.flat(2).length);
    result.forEach((path, index) => expect(_.get(customSeeds, path)).toEqual(index + 1));
    done();
  });

  // Write your own tests

  it.only('should not interpret empty pattern has loop token', async done => {
    const pattern = '';
    const customSeeds: any[] = [];
    const result = listPathsByPattern(customSeeds, pattern);
    expect(result).toEqual([])
    done();
  });

  it.only('should not return value has not pattern found', async done => {
    const pattern = 'must.be.failed';
    const customSeeds = {};
    const result = listPathsByPattern(customSeeds, pattern);
    expect(result).toEqual([])
    done();
  });

  it.only('should return pattern when is found without token loop', async done => {
    const pattern = 'must.be.success';
    const customSeeds = { must: {be: {success: true}}};
    const result = listPathsByPattern(customSeeds, pattern);
    expect(result).toEqual([pattern])
    done();
  });

  it('Should loop over an little array', async done => {
    const pattern = '[*].value';
    const customSeeds = [
      {
        name: 'Random name',
        value: 'hello'
      },
      {
        name: 'Random name 2',
        value: 'world'
      },
      {
        name: 'Random name 3'
      }
    ];
    const expected = ['[0].value', '[1].value'];

    const result = listPathsByPattern(customSeeds, pattern);
    expect(result.length).toEqual(expected.length);
    expect(result).toEqual(expected);
    done();
  });
});
